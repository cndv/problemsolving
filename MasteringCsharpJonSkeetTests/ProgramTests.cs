﻿using MasteringCsharpJonSkeet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NUnit.Framework;


namespace MasteringCsharpJonSkeet.Tests
{
    [TestFixture]
    public class ProgramTests
    {
        [Test]
        public void FooTest()
        {

            /*
             * 
             * int = System.Int32
             * uint = System.Uint32
             * 
             * long = System.Int64
             * ulong = System.UInt64
             * 
             * byte = System.Byte (8 bits)
             * sbyte = System.SByte (8 bits)
             * 
             * short = System.Int16
             * ushort = System.Uint16
             * 
             */

            int x = 10;
            x = x++ + ++x;

            int y = 10;
            y = ++y + y++;

            int z = y << 2 + 5;

            Console.WriteLine(x);
            Console.WriteLine(y);
            Console.WriteLine(z);
        }

        [Test]
        public void SimpleTypes()
        {
            float v1 = 0.2f;
            double v2 = 0.2f;


            float x = 0.1f;
            x += 0.000001f;
            x += 0.000001f;
            x += 0.000001f;
            x += 0.000001f;
            x += 0.000001f;

            Assert.AreNotEqual(0.100005f, x);
        }

        [Test]
        public void NullTermination()
        {
            char[] arrr = {'h', 'e', 'l', 'l', 'o', '\0', 'w'};
            System.Windows.Forms.MessageBox.Show("hello\0world");
            string s = "hi";
            char[] sarr = s.ToCharArray();
            Console.Write(sarr.Length);
        }


        [Test]
        public void RegexTest()
        {
            Regex pattern = new Regex(" ");
            string text = "jon skeet";
            string[] words = pattern.Split(text);
            Assert.AreEqual("jon", words[0]);
        }
    }
}