﻿using PIE_ARR_STR_Problems;
using System;
using System.Collections.Generic;
using System.Text;
using static PIE_ARR_STR_Problems.Utilities;
using static System.Int32;

namespace Trees_Graphs
{
    public class Graph
    {
        //private List<GraphNode> _nodes;
        public Dictionary<int, GraphNode> Nodes;
        public List<Int32> VisitedNodes { get; set; }

        public Graph()
        {
            Nodes = new Dictionary<int, GraphNode>();
            VisitedNodes = new List<int>();
        }
        public void BuildGraphFromArray(string filePath)
        {
            var lines = Utilities.ReadFileFromPath(filePath);

            foreach (var lineToRead in lines)
            {
                // Create a node 
                var node = lineToRead.Split(':');
                TryParse(node[0], out int nodeInt);
                var n = new GraphNode(nodeInt);

                // Add children
                var children = node[1].Split(',');
                if (children[0] != " ")
                {
                    foreach (var c in children)
                    {
                        TryParse(c, out int n32);
                        n.AddAChild(new GraphNode(n32));
                    }
                }

                // Add node to nodes
                Nodes.Add(nodeInt, n);                
            }
        }

        public void PrintGraph()
        {
            Println("Graph Nodes ");
            Println("Parent: Children");

            foreach (var n in Nodes.Values)
            {
                Print(n.Value+" : ");
                if (n.Children != null)
                {
                    foreach (var c in n.Children)
                    {
                       Print(c.Value + " ");
                    }
                }
               
                Println("");
            }
        }

        public GraphNode GetNode(int key)
        {
            return Nodes[key];
        }

        public void PrintVisitedNodes()
        {
            Println("Visited Nodes: ");
            foreach (var n in VisitedNodes)
            {
                Print($"Nodes: {n}, ");
            }
        }

        public void DFS(GraphNode current)
        {
            if (current.Visited == true)
            {
                return;
            }
            else
            {
                Print(current.Value);
                current.Visited = true;
                this.VisitedNodes.Add(current.Value);

                if (current.Children == null) return;

                foreach (var c in current.Children)
                {
                    DFS(this.GetNode(c.Value));
                }
            }
        }

        public void BFS(GraphNode startNode)
        {
            var notVisitedNodesQueue = new Queue<GraphNode>();
            // Enqueue first node
            notVisitedNodesQueue.Enqueue(startNode);

            while (notVisitedNodesQueue.Count != 0)
            {
                var current = GetNode(notVisitedNodesQueue.Dequeue().Value);

                Print(current.Value);

                VisitedNodes.Add(current.Value);
                current.Visited = true;

                if (current.Children == null ) continue;

                foreach (var child in current.Children)
                {
                    var realChild = GetNode(child.Value);
                    if (realChild.Visited == false)
                    {
                        realChild.Visited = true;
                        notVisitedNodesQueue.Enqueue(realChild);
                    }
                }
            }
        }

        // todo: stop when node is visited
        public bool CheckIfRouteExistsBetweenNodes(GraphNode a, GraphNode b)
        {
            BFS(a);
            return VisitedNodes.Contains(b.Value);            
        }

        // Breadth First Search: Shortest Reach
        // https://www.hackerrank.com/challenges/bfsshortreach

        public string BFSShortestReach()
        {
            // Open File
            // Read File
            var filePath = "C:\\Users\\nskostas\\Documents\\_playground\\Workspace\\C# code\\Problem Solving\\CodePractice\\Trees_GraphsTests\\bin\\Debug\\Input.txt";
            var lines = Utilities.ReadFileFromPath(filePath);
            StringBuilder result = new StringBuilder();
            var index = 0;
            Int32.TryParse(lines[index++], out int queries);

            // calculate shortest path
            // println result
            for (int i = 0; i < queries; i++)
            {
                GraphNode startNode;
                var g = ParseGraph(lines, ref index, out startNode);
                var costs = calculatePathCosts(g, startNode);
                
                string s = string.Join("", costs);

                result.Append(s+"\n");
            }
            return result.ToString();
        }

        private static Graph ParseGraph(List<string> lines, ref int index, out GraphNode startingNode)
        {
            var graph = lines[index++].Split(' ');
            Int32.TryParse(graph[0], out int n);
            Int32.TryParse(graph[1], out int m);

            // init graph
            var g = new Graph();
            
            // init empty nodes
            for (int i = 1; i <n+1; i++) g.Nodes.Add(i, new GraphNode(i));
            
            // add edges
            for (int j = 0; j < m; j++)
            {
                var edges = lines[index++].Split(' ');
                Int32.TryParse(edges[0], out int u);
                Int32.TryParse(edges[1], out int v);

                var from = g.GetNode(u);
                var to = g.GetNode(v);
                from.AddAChild(to);
                to.AddAChild(from);
            }

            Int32.TryParse(lines[index++], out int s);
            startingNode = g.GetNode(s);
            return g;
        }

        private static List<int> calculatePathCosts(Graph graph, GraphNode startNode)
        {
            var costs = new List<int>();

            // init to -1 as unreachable 
            for (var i=0; i < graph.Nodes.Count; i++)
            {
                costs.Add(-1);
            }

            // BFS Shortest Paths

            var notVisitedNodesQueue = new Queue<GraphNode>();
            // Enqueue first node
            notVisitedNodesQueue.Enqueue(startNode);

            var cost = 0;
            costs[startNode.Value - 1] = cost;

            while (notVisitedNodesQueue.Count != 0)
            {
                var current = graph.GetNode(notVisitedNodesQueue.Dequeue().Value);

                graph.VisitedNodes.Add(current.Value);
                current.Visited = true;
                costs[current.Value - 1] = current.CostToPath;

                if (current.Children == null) continue;                

                foreach (var child in current.Children)
                {
                    var realChild = graph.GetNode(child.Value);
                    if (realChild.Visited == false)
                    {
                        realChild.Visited = true;
                        realChild.CostToPath = current.CostToPath + 6;
                        notVisitedNodesQueue.Enqueue(realChild);
                    }
                }
            }

            costs.RemoveAt(startNode.Value-1);

            return costs;
        }

        public string DijkstraShortestReach()
        {
            throw new NotImplementedException();
            // Problem
            // 1. Represent weight of an edge in graph
                // Solution Convert List to dictionary with key => node, value => weight
            // 2. Implement Dijkstra's algorithm
                
        }
    }
}
