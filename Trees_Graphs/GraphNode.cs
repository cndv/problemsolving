﻿using System.Collections.Generic;

namespace Trees_Graphs
{
    public class GraphNode
    {
        public  int Value { get; private set; }
        public bool Visited { get; set; }
        public  List<GraphNode> Children { get; private set; }
        public int CostToPath { get; set; }

        public GraphNode(int value)
        {
            Value = value;
            Children = null;
            Visited = false;
            CostToPath = 0;
        }

        public void AddAChild(GraphNode n)
        {
            if (Children == null) Children = new List<GraphNode>();
            if (Children.Contains(n)) return;
            Children.Add(n);
        }
    }
}