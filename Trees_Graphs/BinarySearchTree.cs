﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PIE_ARR_STR_Problems.Utilities;

namespace Trees_Graphs
{
    public class BinarySearchTree
    {
        public TreeNode Root { get; set; }

        // current -> left -> right
        public void Preorder(TreeNode current)
        {            
            if (current != null)
            {
                Print($"{current._value} ");
                Preorder(current._left);                
                Preorder(current._right);
            }
        }

        // left -> current -> right
        public void Inorder(TreeNode current)
        {
            if (current != null)
            {
                Inorder(current._left);
                Print($"{current._value} ");
                Inorder(current._right);
            }
        }

        // left -> right -> current
        public void Postorder(TreeNode current)
        {
            if (current != null)
            {
                Postorder(current._left);
                Postorder(current._right);
                Print($"{current._value} ");
            }
        }

        public TreeNode FindNode(int val)
        {
            var tmp = Root;
            while (tmp != null)
            {
                if (tmp._value == val)
                {
                    return tmp;
                }
                else if (val < tmp._value)
                {
                    tmp = tmp._left;
                }
                else
                {
                    tmp = tmp._right;
                }                
            }                
            return null;
        }

        public virtual void InsertNode(TreeNode n)
        {
            if (Root == null)
            {
                Root = n;
            }
            else 
            {
                var tmp = Root;
                while (tmp != null)
                {
                    if (n._value < tmp._value)
                    {
                        if (tmp._left == null)
                        {
                            tmp._left = n;
                            n._parent = tmp;
                            break;
                        }
                        else
                        {
                            tmp = tmp._left;
                        }                        
                    }
                    else
                    {
                        if (tmp._right == null)
                        {
                            tmp._right = n;
                            n._parent = tmp;
                            break;
                        }
                        else
                        {
                            tmp = tmp._right;
                        }                        
                    }
                }
            }
        }

        public static int GetHeightOfTree(TreeNode root)
        {
            if (root == null)
            {
                return 0;
            }
            else
            {
                return Math.Max(GetHeightOfTree(root._left), GetHeightOfTree(root._right)) + 1;
            }
        }

        public virtual void BuildBstFromArray(int[] A)
        {
            foreach (var num in A)
            {
                InsertNode(new TreeNode(num));
            }
        }

        public void PrintBst()
        {
            Inorder(Root);
        }

        // todo
        public void DeleteNode(int x)
        {

        }

        /// <summary>
        /// Given a binary tree, design an algorithm which creates a linked
        /// list of all the nodes at each depth. (if depth = D, => there are D linked lists
        /// </summary>
        public List<List<int>> ListOfDepths()
        {
            //create a hashmap with id the depth and parse the tree in whatever order
            throw new NotImplementedException();
        }

        public bool ValidateBst(TreeNode n)
        {
            return ValidateBst(n, null, null);
        }

        public bool ValidateBst(TreeNode n, int? min, int? max)
        {
            if (n == null)
            {
                return true;
            }

            if ((min != null && n._value <= min) || (max != null && n._value > max))
            {
                return false;
            }

            if (!ValidateBst(n._left, min, n._value) || !ValidateBst(n._right, n._value, max))
            {
                return false;
            }

            return true;
        }

        //
        // Given a current in order node return the next node
        public TreeNode FindNextInOrderSuccessor(TreeNode current)
        {
            if (current._left != null)
            {
                return current._left;
            }
            else if (current._right != null)
            {
                return current._right;
            }
            else
            {
                if (current == current._parent._left)
                {
                    return current._parent;
                }
                else
                {
                    return current._parent._parent;
                }
            }
        }

        /*
         * BST Sequences: A binary search tree was created by traversing through an array from left to right
         *                and inserting each element. Given a binary search tree with distinct elements, print all possible
         *                arrays that could have led to this tree.
         */

        public List<Array> FindBSTSequences()
        {
            throw new NotImplementedException();
            // Logic
            // find all lists per dep=y7th
            // foreach list find all possible combinations
            // merge all sets of next level to sets of previous level starting from root
        }

        public List<Array> FindAllCombinations(int[] A)
        {
            throw new NotImplementedException();
        }

        public static void Main() { }
    }
}
