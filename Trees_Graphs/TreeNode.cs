﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trees_Graphs
{
    public class TreeNode
    {
        public int _value { get; set; }
        public int _height { get; set; }
        public TreeNode _left { get; set; }
        public TreeNode _right { get; set; }
        public TreeNode _parent { get; set; }

        public TreeNode(int n = 0)
        {
            _value = n;
            _height = 0;            
            _left = null;
            _right = null;
            _parent = null;
        }
    }
}
