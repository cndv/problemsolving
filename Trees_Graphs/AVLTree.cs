﻿using System;

namespace Trees_Graphs
{
    public enum Direction
    {
        Left, Right
    }

    public enum BalanceFactor
    {
        MaxFactor = 2
    }

    public class AVLTree: BinarySearchTree
    {

        //todo
        public override void BuildBstFromArray(int[] A)
        {
            foreach (var a in A)
            {
                InsertNode(new TreeNode(a));    
            }
        }

        //todo
        public override void InsertNode(TreeNode newNode)
        {
            base.InsertNode(newNode);
            BalanceTree(newNode);
        }

        private void BalanceTree(TreeNode n)
        {
            var tmp = n;
            while (tmp._parent != null)
            {
                tmp = CheckIfTreeIsBalanced(tmp) ? tmp._parent : Rotate(tmp);
            }
        }

        private static TreeNode Rotate(TreeNode n)
        {
            if (BalanceFactor(n) > 0)
            {
                if (BalanceFactor(n._right) > 0)
                {
                    RotateLeft(n);
                }
                else
                {
                    RotateRight(n._right);
                    RotateLeft(n);
                }
            }
            else
            {
                if (BalanceFactor(n._left) < 0)
                {
                    RotateRight(n);
                }
                else
                {
                    RotateLeft(n._left);
                    RotateRight(n);
                }
            }
            return n;
        }

        private static void RotateLeft(TreeNode n)
        {
            var tmp = n._right;
            n._right = tmp._left;
            tmp._left = n;
            tmp._parent = n._parent;
            n._parent = tmp;
        }

        private static void RotateRight(TreeNode n)
        {
            var tmp = n._left;
            n._left = tmp._right;
            tmp._right = n;
            tmp._parent = n._parent;
            n._parent = tmp;
        }

        public static bool CheckIfTreeIsBalanced(TreeNode n)
        {
            return BalanceFactor(n) < Math.Abs((int)Trees_Graphs.BalanceFactor.MaxFactor);
        }

        public static int BalanceFactor(TreeNode n)
        {
            return GetHeightOfTree(n._right) - GetHeightOfTree(n._left);
        }
        //todos
        public TreeNode Search(TreeNode n)
        {
            throw new NotImplementedException();
        }
        //todo
        public void Delete(TreeNode n)
        {
            throw new NotImplementedException();
        }
        //todo
        public void Rotate(TreeNode n, Direction d)
        {
            throw new NotImplementedException();
        }
    }
}
