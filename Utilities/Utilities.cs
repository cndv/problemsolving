﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PIE_ARR_STR_Problems
{
    public static class Utilities
    {
        public static void Print<T>(T outValue)
        {
            Console.Write($"{outValue}");
        }

        public static void Println<T>(T outValue)
        {
            Console.WriteLine($"{outValue}");
        }

        public static void Pause()
        {
            Console.ReadKey();
        }    

        public static string Dumper(this object value)
        {
            return JsonConvert.SerializeObject(value, Formatting.Indented);
        }

        public static void Dump(this object value)
        {
            Print(JsonConvert.SerializeObject(value, Formatting.Indented)); 
        }

        public static int[] GetRandomArray(int size, int maxRange)
        {
            int[] A = new int[size];

            var r = new Random();
            for (int i = 0; i < size; i++)
            {
                A[i] = r.Next(maxRange);
            }

            return A;
        }

        public static List<string> ReadFileFromPath(string filePath)
        {
            var lines = new List<string>();
            using (var sr = new StreamReader(filePath))
            {
                string lineToRead;
                while ((lineToRead = sr.ReadLine()) != null)
                {
                    lines.Add(lineToRead);
                }
            }
            return lines;
        }

        public static void PrintList<T>(List<T> A)
        {
            foreach (var o in A)
            {
                Print(o.ToString() +" ");
            }
            Println("");
        }

        public static void Main()
        {
            GetRandomArray(10,10).Dump();
            Pause();
        }

        public static bool CompareResultsFromFile(string inputFilePath, string outputFilePath)
        {
            var input = ReadFileFromPath(inputFilePath);
            var output = ReadFileFromPath(outputFilePath);
            return input.SequenceEqual(output);
        }
    }
}
