﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HackerRank
{
    public class Graph
    {
        //private List<GraphNode> _nodes;
        public Dictionary<int,GraphNode> Nodes;
        public List<Int32> VisitedNodes { get; set; }

        public Graph()
        {
            Nodes = new Dictionary<int, GraphNode>();
            VisitedNodes = new List<int>();
        }

        // todo this needs optimisation, use HASH
        public GraphNode GetNode(int key)
        {
            return Nodes[key];
        }

        public static Graph ParseGraph( out GraphNode startingNode)
        {
            var graphToRead = Console.ReadLine();
            var graph = graphToRead.Split(' ');
            int n, m;
            Int32.TryParse(graph[0], out n);
            Int32.TryParse(graph[1], out m);

            // init graph
            var g = new Graph();

            // init empty nodes
            for (int i = 1; i < n + 1; i++) g.Nodes.Add(i, new GraphNode(i));

            // add edges
            for (int j = 0; j < m; j++)
            {
                var edgesToRead = Console.ReadLine();
                var edges = edgesToRead.Split(' ');
                int u, v;
                Int32.TryParse(edges[0], out u);
                Int32.TryParse(edges[1], out v);

                var from = g.GetNode(u);
                var to = g.GetNode(v);
                from.AddAChild(to);
                to.AddAChild(from);
            }

            int s;
            var sToRead = Console.ReadLine();
            Int32.TryParse(sToRead, out s);
            startingNode = g.GetNode(s);
            return g;
        }

        public static List<int> calculatePathCosts(Graph graph, GraphNode startNode)
        {
            var costs = new List<int>();

            // init to -1 as unreachable 
            for (var i = 0; i < graph.Nodes.Count; i++)
            {
                costs.Add(-1);
            }

            // BFS Shortest Paths

            var notVisitedNodesQueue = new Queue<GraphNode>();
            // Enqueue first node
            notVisitedNodesQueue.Enqueue(startNode);

            var cost = 0;
            costs[startNode.Value - 1] = cost;

            while (notVisitedNodesQueue.Count != 0)
            {
                var current = graph.GetNode(notVisitedNodesQueue.Dequeue().Value);

                graph.VisitedNodes.Add(current.Value);
                current.Visited = true;
                costs[current.Value - 1] = current.CostToPath;

                if (current.Children == null) continue;

                foreach (var child in current.Children)
                {
                    var realChild = graph.GetNode(child.Value);
                    if (realChild.Visited == false)
                    {
                        realChild.Visited = true;
                        realChild.CostToPath = current.CostToPath + 6;
                        notVisitedNodesQueue.Enqueue(realChild);
                    }
                }
            }

            costs.RemoveAt(startNode.Value - 1);

            return costs;
        }
    }

    public class GraphNode
    {
        public int Value { get; private set; }
        public bool Visited { get; set; }
        public List<GraphNode> Children { get; private set; }

        public int CostToPath { get; set; }

        public GraphNode(int value)
        {
            Value = value;
            Children = null;
            Visited = false;
            CostToPath = 0;
        }

        public void AddAChild(GraphNode n)
        {
            if (Children == null) Children = new List<GraphNode>();
            if (Children.Contains(n)) return;
            Children.Add(n);
        }
    }

    class Solution
    {
        static void Main(string[] args)
        {
                StringBuilder result = new StringBuilder();
                int queries;
                var numberOfQueries = Console.ReadLine();
                Int32.TryParse(numberOfQueries, out queries);

                // calculate shortest path
                // println result
                for (int i = 0; i < queries; i++)
                {
                    GraphNode startNode;                    
                    var g = Graph.ParseGraph(out startNode);
                    var costs = Graph.calculatePathCosts(g, startNode);

                    string s = string.Join(" ", costs);

                    result.Append(s + "\n");
                }
                Console.WriteLine(result.ToString());
        }
    }
}