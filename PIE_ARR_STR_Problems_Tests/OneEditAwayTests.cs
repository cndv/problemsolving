﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIE_ARR_STR_Problems.Tests
{
    [TestClass()]
    public class OneEditAwayTests
    {
        [TestMethod()]
        public void OneEditAway2Test()
        {
            Assert.AreEqual(OneEditAway.OneEditAway2("apsb", "aple"), false);
        }
    }
}