﻿using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace PIE_ARR_STR_Problems.Tests
{
    [TestClass()]
    public class SolutionTests
    {
        [TestMethod()]
        public void getFirstNonRepeatedCharacterTest()
        {     
            Assert.AreEqual(FirstNonRepeatedChar.getFirstNonRepeatedCharacter("total"), 'o');
            Assert.AreEqual(FirstNonRepeatedChar.getFirstNonRepeatedCharacter("teeter"), 'r');
        }

        [TestMethod()]
        public void getFirstNonRepeatedCharacter2Test()
        {         
            Assert.AreEqual(FirstNonRepeatedChar.getFirstNonRepeatedCharacter2("total"), 'o');
            Assert.AreEqual(FirstNonRepeatedChar.getFirstNonRepeatedCharacter2("teeter"), 'r');
        }

      
    }
}