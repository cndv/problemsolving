﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PIE_ARR_STR_Problems.Tests
{
    [TestClass()]
    public class FindSumOfPairsTests
    {
        [TestMethod()]
        public void findAMathingPairTest()
        {          
            Tuple<int, int> t1 = Tuple.Create(-1,-1);
            int[] ar1 = { 1, 2, 3, 9 };
            Assert.AreEqual(FindSumOfPairs.findAMathingPair(ar1,8), t1);            

            Tuple<int, int> t2 = Tuple.Create(2, 3);
            int[] ar2 = { 1, 2, 4, 4 };
            Assert.AreEqual(FindSumOfPairs.findAMathingPair(ar2,8), t2);
        }

        [TestMethod()]
        public void findAMathingPair2Test()
        {
            Tuple<int, int> t1 = Tuple.Create(-1, -1);
            int[] ar1 = { 9, 2, 1, 3 };
            Assert.AreEqual(FindSumOfPairs.findAMathingPair2(ar1, 8), t1);

            Tuple<int, int> t2 = Tuple.Create(2, 3);
            int[] ar2 = { 4, 1, 4, 2 };
            Assert.AreEqual(FindSumOfPairs.findAMathingPair2(ar2, 8), t2);
        }
    }
}