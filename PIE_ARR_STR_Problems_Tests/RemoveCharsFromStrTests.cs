﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PIE_ARR_STR_Problems.Tests
{
    [TestClass()]
    public class RemoveCharsFromStrTests
    {
        [TestMethod()]
        public void removeSpecifiedCharacterFromStringTest()
        {
            string input = "Hello World";
            string charactersToRomove = "oe";
            string result = RemoveCharsFromStr.removeSpecifiedCharacterFromString(input, charactersToRomove);

            Assert.AreEqual("Hll Wrld", result);            
        }
    }
}