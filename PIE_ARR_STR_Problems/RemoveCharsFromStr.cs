﻿using System;
using System.Text.RegularExpressions;

namespace PIE_ARR_STR_Problems
{
    public class RemoveCharsFromStr
    {
        public static string removeSpecifiedCharacterFromString(string input, string charactersToRemove)
        {
            string replacement = "";

            char[] charsArr = charactersToRemove.ToCharArray();

            string charsToRemovePattern = String.Join("|", charsArr);

            string result = Regex.Replace(input, charsToRemovePattern, replacement);

            return result;
        }
    }
}
