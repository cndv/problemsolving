﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIE_ARR_STR_Problems
{

    /*
     * Problem: Given an array of ints, find a matching pair that is equal to a sum
     *          Array is sorted
     *          
     *          What if the array isn't sorted.
     *              Sort the array and reaply algorithm nlogn faster ? no additional ds ?
     *              
     *          
     * Case1: [1,2,3,9] -> No
     * Case2: [1,2,4,4] -> Yes [4,4]
     * 
     * Solution:
     *  1. Scan every num with all others n^2
     *  2. use two indices and compare
     */
    public class FindSumOfPairs
    {
      

        public static Tuple<int, int> findAMathingPair(int[] arr, int sum)
        {
            Tuple<int, int> t = Tuple.Create(-1,-1);

            int i = 0;
            int j = arr.Length - 1;
            int tempSum = 0;

            while (i < j && i < arr.Length && j >= 0)
            {
                tempSum = arr[i] + arr[j];
                if (tempSum == sum)
                {
                    Tuple<int, int> resultTuple = Tuple.Create(i, j);
                    return resultTuple;
                }
                else if (tempSum < sum)
                {
                    i++;
                }
                else
                {
                    j--;
                }
            }

            return t;
        }

        // array is not sorted
        // with complements
        //
        public static Tuple<int, int> findAMathingPair2(int[] arr, int sum)
        {
            Tuple<int, int> t = Tuple.Create(-1, -1);
            return t;
        }
    }
}
