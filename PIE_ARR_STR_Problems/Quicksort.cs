﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIE_ARR_STR_Problems
{
    class Quicksort
    {
        public static void quicksort(int[] A, int left, int right)
        {
            int i = partition(A, left, right);

            if (i-1 > left) { quicksort(A, left, i-1); }
            if (i < right) { quicksort(A, i, right); }
        }

        private static int partition(int[] a, int left, int right)
        {
            int pivot = a[(left + right) / 2];

            while (left < right)
            {
                while (a[left] < pivot) left++;
                while (a[right] > pivot) right--;

                if (left <= right)
                {
                    swap(a, left, right);
                    left++;
                    right--;
                }
            }
            return left;
        }

        private static void swap(int[] a, int left, int right)
        {
            int t = a[left];
            a[left] = a[right];
            a[right] = t;
        }

        /*
        public static void Main()
        {
            
            Random randNum = new Random();
            int[] A = Enumerable
                .Repeat(0, 3)
                .Select(i => randNum.Next(1, 9))
                .ToArray();
            int[] B = { 8, 1, 6, 2};

            Console.WriteLine("Unsorted Array: " + string.Join(",",B));

            quicksort(B, 0, 3);

            Console.WriteLine("Sorted Array: " + string.Join(",", B));

            Console.ReadLine();
        }

        */
    }
}
