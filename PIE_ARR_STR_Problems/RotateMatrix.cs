﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIE_ARR_STR_Problems
{
    class RotateMatrix
    {
        private static readonly Random RandomIntNumbers = new Random();

        public static int RandomInt(int n)
        {
            return RandomIntNumbers.Next(n);
        }

        public static int RandomIntInRange(int min, int max)
        {
            return RandomInt(max + 1 - min) + min;
        }

        public static bool RandomBoolean()
        {
            return RandomIntInRange(0, 1) == 0;
        }

        public static bool RandomBoolean(int percentTrue)
        {
            return RandomIntInRange(1, 100) <= percentTrue;
        }

        public static int[][] RandomMatrix(int m, int n, int min, int max)
        {
            int[][] matrix = new int[m][];
            for (int i = 0; i < m; i++)
            {
                matrix[i] = new int[n];
                for (int j = 0; j < n; j++)
                {
                    matrix[i][j] = RandomIntInRange(min, max);
                }
            }
            return matrix;
        }

        public static int[] RandomArray(int N, int min, int max)
        {
            int[] array = new int[N];
            for (int j = 0; j < N; j++)
            {
                array[j] = RandomIntInRange(min, max);
            }
            return array;
        }

        public static void PrintMatrix(int[][] matrix)
        {
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[i].Length; j++)
                {
                    if (matrix[i][j] < 10 && matrix[i][j] > -10)
                    {
                        Console.Write(" ");
                    }
                    if (matrix[i][j] < 100 && matrix[i][j] > -100)
                    {
                        Console.Write(" ");
                    }
                    if (matrix[i][j] >= 0)
                    {
                        Console.Write(" ");
                    }
                    Console.Write(" " + matrix[i][j]);
                }
                Console.WriteLine();
            }
        }


        public static void Rotate(int[][] matrix, int n)
        {

            for (var layer = 0; layer < n / 2; ++layer)
            {
                var first = layer;
                var last = n - 1 - layer;

                for (var i = first; i < last; ++i)
                {
                    var offset = i - first;
                    var top = matrix[first][i]; // save top

                    // left -> top
                    // top = left
                    var left = matrix[last - offset][first];
                    matrix[first][i] = matrix[last - offset][first];

                    // bottom -> left
                    // left = bottom
                    var bottom = matrix[last][last - offset];
                    matrix[last - offset][first] = matrix[last][last - offset];

                    // right -> bottom
                    // bottom = right
                    var right = matrix[i][last];
                    matrix[last][last - offset] = matrix[i][last];

                    // top -> right
                    // right = top
                    matrix[i][last] = top; // right <- saved top
                }
            }
        }

        public static void Run()
        {
            const int size = 4;

            var matrix = RandomMatrix(size, size, 0, 9);

            PrintMatrix(matrix);

            Rotate(matrix, size);
            Console.WriteLine();
            PrintMatrix(matrix);
        }

        //static void Main(string[] args)
        //{
        //    Run();
        //}
    }
}
