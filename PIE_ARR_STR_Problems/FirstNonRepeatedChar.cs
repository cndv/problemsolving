﻿using System;
using System.Collections;
using System.Collections.Specialized;

namespace PIE_ARR_STR_Problems
{
    /*
     * Definition: Implement a function to return the first non repeated character in a given string
     */


    public class FirstNonRepeatedChar
    {
        // solution approach
        // 1. naive Compare every character with the next ones, if no match return it
        // 2. scan and pass every char to a dict by mainting order of input
        //      iterate dict by key and print first key that has value of 1
        // 3. sultion withouth using an external ds: only step 1.


        // 1. naive
        static public char getFirstNonRepeatedCharacter(String testcase)
        {
            char[] testcaseToArray = testcase.ToCharArray();
            char result = '\0';
            char temp = testcaseToArray[0];
            char temp2 = temp;

            for (int i= 0; i < testcaseToArray.Length; i++)
            {
                bool single = true;
                for (int j=0; j < testcaseToArray.Length; j++)
                {
                    if (j==i)
                    {
                        continue;
                    }
                    else if (testcaseToArray[i] == testcaseToArray[j])
                    {
                        single = false;
                        break;
                    }                    
                }
                if (single)
                {
                    return testcaseToArray[i];
                }
            }

            return result;
        }


        //2. 
        static public char getFirstNonRepeatedCharacter2(String testcase)
        {
            char firstCharacter = '\0';
            char[] testcaseToArray = testcase.ToCharArray();
            OrderedDictionary od = new OrderedDictionary();
            
            foreach (char c in testcaseToArray)
            {
                if (od.Contains(c))
                {
                    var val = od[(object)c];
                    int ival = Convert.ToInt32(val);
                    ival++;
                    od[(object)c] = (object)ival;
                    
                }
                else
                {
                    od.Add(c,1);
                }
            }

            foreach (DictionaryEntry d in od)
            {
                if (Convert.ToInt32(d.Value) == 1)
                {
                    return Convert.ToChar(d.Key);
                }
            }
           

            return firstCharacter;
            
        }

     
    }
}
