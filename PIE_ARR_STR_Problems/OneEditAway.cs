﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIE_ARR_STR_Problems
{
    public class OneEditAway
    {
            /* Check if you can insert a character into s1 to make s2. */

            public static bool OneEditInsert(String s1, String s2)
            {
                int index1 = 0;
                int index2 = 0;
                while (index2 < s2.Length && index1 < s1.Length)
                {
                    if (s1[index1] != s2[index2])
                    {
                        if (index1 != index2)
                        {
                            return false;
                        }
                        index2++;
                    }
                    else
                    {
                        index1++;
                        index2++;
                    }
                }
                return true;
            }

            public static bool OneEditAway2(String first, String second)
            {
                if (first.Length == second.Length)
                {
                    return OneEditInsert(first, second);
                }
                else if (first.Length + 1 == second.Length)
                {
                    return OneEditInsert(first, second);
                }
                else if (first.Length - 1 == second.Length)
                {
                    return OneEditInsert(second, first);
                }
                return false;
            }

            public static bool OneEditAway22(String first, String second)
            {
                /* Length checks. */
                if (Math.Abs(first.Length - second.Length) > 1)
                {
                    return false;
                }

                /* Get shorter and longer string.*/
                String s1 = first.Length < second.Length ? first : second;
                String s2 = first.Length < second.Length ? second : first;

                int index1 = 0;
                int index2 = 0;
                bool foundDifference = false;
                while (index2 < s2.Length && index1 < s1.Length)
                {
                    if (s1[index1] != s2[index2])
                    {
                        /* Ensure that this is the first difference found.*/
                        if (foundDifference) return false;
                        foundDifference = true;
                        if (s1.Length == s2.Length)
                        { // On replace, move shorter pointer
                            index1++;
                        }
                    }
                    else
                    {
                        index1++; // If matching, move shorter pointer
                    }
                    index2++; // Always move pointer for longer string
                }
                return true;
            }

            public void Run()
            {
                String a = "apples";
                String b = "aple";
                /*bool isOneEdit = OneEditAway22(a, b);
                Console.WriteLine("{0}, {1}: {2}", a, b, isOneEdit);

                bool isOneEdit2 = OneEditAway2(a, b);
                Console.WriteLine("{0}, {1}: {2}", a, b, isOneEdit2);
                */
            }
        }
    }



