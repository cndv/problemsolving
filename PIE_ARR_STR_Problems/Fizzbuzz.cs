﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIE_ARR_STR_Problems
{
    public class Fizzbuzz
    {
        /*
         * print numbers from 1..100 
         * for multiples of 3 print fizz
         * for multiples of 5 print buzz
         * for multipes of 3 and 5 print fizzbuzz
         * other wise print the number
         */
        static void printFizzbuzz()
        {
            for (int i = 1; i <= 100; i++)
            {
                if (i % 15 == 0) // not very clear
                {
                    Console.Write(i+" ");
                    Console.WriteLine("fizzbuzz");
                }
                else if (i % 3 == 0)
                {
                    Console.Write(i + " ");
                    Console.WriteLine("fizz");
                }
                else if (i % 5 == 0)
                {
                    Console.Write(i + " ");
                    Console.WriteLine("buzz");
                }
                else
                {
                    Console.WriteLine(i);
                }
            }
        }


        static void printFizzBuzz2()
        {
            for (int i = 1; i <= 100; i++)
            {
                bool fizz = (i % 3 == 0);
                bool buzz = (i % 5 == 0);

                if (fizz && buzz)
                {
                    Console.Write(i + " ");
                    Console.WriteLine("fizzbuzz");
                }
                else if (fizz)
                {
                    Console.Write(i + " ");
                    Console.WriteLine("fizz");
                }
                else if (buzz)
                {
                    Console.Write(i + " ");
                    Console.WriteLine("buzz");
                }
                else
                {
                    Console.WriteLine(i);
                }
            }
        }


        static void fizzbuzzLINQ()
        {
            Console.WriteLine(
            String.Join(
              Environment.NewLine,
              Enumerable.Range(1, 100)
                .Select(n => n % 15 == 0 ? "FizzBuzz"
                           : n % 3 == 0 ? "Fizz"
                           : n % 5 == 0 ? "Buzz"
                           : n.ToString())
            ));
        }

        public static void Main()
        {
            printFizzBuzz2();
            Console.ReadKey();
        }
    }
}
