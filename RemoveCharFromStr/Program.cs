﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

/*
 * Remove specified characters from string 
 * 
 */


namespace RemoveCharFromStr
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = "Hello World";

            string charactersToRomove = "oe";

            string result = removeSpecifiedCharacterFromString(input, charactersToRomove);

            Console.WriteLine("Original String: {0}", input);
            Console.WriteLine("Replacement String: {0}", result);
            Console.ReadKey();
        }




        public static string removeSpecifiedCharacterFromString(string input, string charactersToRemove)
        {
            string replacement = "";

            char[] charsArr = charactersToRemove.ToCharArray();

            string charsToRemovePattern = String.Join("|", charsArr);

            string result = Regex.Replace(input, charsToRemovePattern, replacement);

            return result;
        }
    }
}
