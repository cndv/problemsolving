﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp.Tests
{
    [TestClass()]
    public class ATests
    {
        [TestMethod()]
        public void RunTest()
        {
            var a = new Node();
            ParameterPassingPlay.ChangeArg(a);
            Assert.AreEqual(0, a.Val);
        }
    }
}