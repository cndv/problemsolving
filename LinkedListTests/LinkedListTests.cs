﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PIE_ARR_STR_Problems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PIE_ARR_STR_Problems.LinkedList;


namespace PIE_ARR_STR_Problems.Tests
{
    [TestClass()]
    public class LinkedListTests
    {
        [TestMethod()]
        public void BuildLinkedListFromArrayTest()
        {
            int[] a = { 7, 1, 6 };
            int[] b = { 5, 9 };

            var la = LinkedList.BuildLinkedListFromArray(a, AppendType.End);
            var lb = LinkedList.BuildLinkedListFromArray(b, AppendType.Front);
            string lstA = la.LinkedlistToString();
            string lstB = lb.LinkedlistToString();
            Assert.AreEqual(lstA, "7 1 6");
            Assert.AreEqual(lstB, "9 5");
        }

        [TestMethod()]
        public void DeleteNodeTest()
        {
            int[] a = { 7, 1, 6 };
            var la = LinkedList.BuildLinkedListFromArray(a, AppendType.End);
            la.DeleteNode(6);
            string lstA = la.LinkedlistToString();

            Assert.AreEqual(lstA, "7 1");
        }

        [TestMethod()]
        public void FindElementTest()
        {
            int[] a = { 7, 1, 6 };
            var la = LinkedList.BuildLinkedListFromArray(a, AppendType.End);
            bool result = la.FindElement(6);
            bool result2 = la.FindElement(3);

            Assert.AreEqual(result, true);
            Assert.AreEqual(result2, false);
        }

        [TestMethod()]
        public void RemoveDuplicatesTest()
        {
            int[] a = { 7, 6, 1, 6 };
            var la = LinkedList.BuildLinkedListFromArray(a, AppendType.End);
            bool result = la.RemoveDuplicates();

            Assert.AreEqual(result, true);

            int[] b = { 7, 1, 6 };
            var lb = LinkedList.BuildLinkedListFromArray(b, AppendType.End);
            bool resultb = lb.RemoveDuplicates();

            Assert.AreEqual(resultb, false);
        }

        [TestMethod()]
        public void RemoveDuplicatesWithoutTempBufferTest()
        {
            int[] a = { 7, 6, 1, 6 };
            var la = LinkedList.BuildLinkedListFromArray(a, AppendType.End);
            bool result = la.RemoveDuplicates();

            Assert.AreEqual(result, true);

            int[] b = { 7, 1, 6 };
            var lb = LinkedList.BuildLinkedListFromArray(b, AppendType.End);
            bool resultb = lb.RemoveDuplicates();

            Assert.AreEqual(resultb, false);
        }

        [TestMethod()]
        public void ReverseLinkedListTest()
        {
            int[] a = { 7, 1, 6, 8, 4 };

            var la = LinkedList.BuildLinkedListFromArray(a, AppendType.End);
            var laReversed = la.ReverseLinkedList();
            var lstA = laReversed.LinkedlistToString();

            Assert.AreEqual(lstA, "4 8 6 1 7");
        }

        [TestMethod()]
        public void GetLengthTest()
        {
            int[] a = { 7, 1, 6, 8, 4 };
            var la = LinkedList.BuildLinkedListFromArray(a, AppendType.End);
            var llength = la.GetLength();
            Assert.AreEqual(llength, a.Length);

            int[] b = { 7 };
            var lb = LinkedList.BuildLinkedListFromArray(b, AppendType.End);
            var llengthb = lb.GetLength();
            Assert.AreEqual(llengthb, b.Length);

            int[] c = { };
            var lc = LinkedList.BuildLinkedListFromArray(c, AppendType.End);
            var llengthc = lc.GetLength();
            Assert.AreEqual(llengthc, c.Length);
        }

        [TestMethod()]
        public void BuildLinkedListFromArrayOnTheFlyTest()
        {
            int[] a = { 7, 1, 6 };
            int[] b = { 5, 9 };

            var la = new LinkedList();

            Appender appendDelegatee = la.AppendToTail;

            BuildLinkedListFromArrayOnTheFly(a, appendDelegatee);


            var lb = new LinkedList();

            Appender appendDelegateeB = lb.AppendToHead;

            BuildLinkedListFromArrayOnTheFly(b, appendDelegateeB);


            string lstA = la.LinkedlistToString();
            string lstB = lb.LinkedlistToString();

            Assert.AreEqual(lstA, "7 1 6");
            Assert.AreEqual(lstB, "9 5");
        }

        [TestMethod()]
        public void PalindromeTest()
        {
            int[] a = { 7, 2, 1, 2, 7 };
            int[] b = { 4, 3, 3, 4 };
            int[] c = { 4, 3, 1, 2, 3, 4 };

            // la Palindrome -> true
            var la = new LinkedList();

            Appender appendDelegatee = la.AppendToTail;

            BuildLinkedListFromArrayOnTheFly(a, appendDelegatee);

            // lb Palindrome -> true
            var lb = new LinkedList();

            Appender appendDelegateeB = lb.AppendToHead;

            BuildLinkedListFromArrayOnTheFly(b, appendDelegateeB);
                
            // lc palindrome -> false
            var lc = new LinkedList();

            Appender appendDelegateeC = lc.AppendToHead;

            BuildLinkedListFromArrayOnTheFly(c, appendDelegateeC);

            Assert.AreEqual(la.Palindrome(), true);
            Assert.AreEqual(lb.Palindrome(), true);
            Assert.AreEqual(lc.Palindrome(), false);
        }

        [TestMethod()]
        public void ReverseListInPlace2Test()
        {
            int[] a = { 7, 1, 6, 8, 4 };

            var la = LinkedList.BuildLinkedListFromArray(a, AppendType.End);
            la.ReverseListInPlace2();
            var result = la.LinkedlistToString();

            Assert.AreEqual("4 8 6 1 7", result);
        }
    }
}