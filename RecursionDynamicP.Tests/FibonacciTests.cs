﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RecursionDynamicP.Tests
{
    [TestClass()]
    public class FibonacciTests
    {
        [TestMethod()]
        public void FibonacciTest()
        {
            var sut = new Fibonacci();

            var result = sut.FibonacciRecursive(39);

            Assert.AreEqual(63245986, result);           
        }


        [TestMethod()]
        public void FibonacciMemoizationTest()
        {
            var sut = new Fibonacci();

            var result = sut.FibonacciMemoization(39);

            Assert.AreEqual(63245986, result);
        }


        [TestMethod()]
        public void FibonacciDynamicProgrammingTest()
        {
            var sut = new Fibonacci();

            var result = sut.FibonacciDynamicProgramming(39);

            Assert.AreEqual(63245986, result);
        }

        [TestMethod()]
        public void FibonacciDynamicProgrammingNoArrayTest()
        {
            var sut = new Fibonacci();

            var result = sut.FibonacciDynamicProgrammingNoArray(39);

            Assert.AreEqual(63245986, result);
        }

    }
}