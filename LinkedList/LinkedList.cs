﻿using System;
using System.Collections;
using static PIE_ARR_STR_Problems.Utilities;


namespace PIE_ARR_STR_Problems
{
	public enum AppendType { Front, End }
	public class LinkedList
	{
		public Node Head { get; set; }

		public delegate void Appender (Node n);

		public class Node
		{
			public int Value { get; set; }
			public Node Next { get; set; }

			public Node()
			{

			}
			public Node(int v)
			{
				Value = v;
				Next = null;
			}

		}

		public LinkedList()
		{
			Head = null;
		}

		public void AppendToTail(Node n)
		{
			if (Head == null)
			{
				Head = n;
			}
			else
			{
				var tmp = Head;

				while (tmp.Next != null)
				{
					tmp = tmp.Next;
				}
				tmp.Next = n;
			}
		}       

		public void AppendToHead(Node n)
		{
			n.Next = Head;
			Head = n;		    
		}

		public string LinkedlistToString()
		{
			var tmp = Head;
			var str = "";

			while (tmp.Next != null)
			{
				str += tmp.Value + " ";
				tmp = tmp.Next;
			}
			str += tmp.Value;
			return str;
		}

		public bool DeleteNode(int x)
		{
			var tmp = Head;

			if (Head == null)
			{
				return false;
			}
			else if (Head.Value == x)
			{
				Head = Head.Next;
				return true;
			}

			while (tmp.Next != null)
			{
				if (tmp.Next.Value == x)
				{
					tmp.Next = tmp.Next.Next;
					return true;
				}

				tmp = tmp.Next;
			}
			return false;
		}

		public bool FindElement(int x)
		{
			var tmp = Head;

			if (Head == null)
			{
				return false;
			} 

			do
			{
				if (tmp.Value == x)
				{
					return true;
				}

				tmp = tmp.Next;

			} while (tmp != null);			

			return false;
		}

		// todo add delegates as parameter 
		public static LinkedList BuildLinkedListFromArray(int[] A, AppendType appendtype)
		{
			var l = new LinkedList();
			/*
			 * *** Initialize an empty array of objects *** 
			 */

			if (A.Length != 0)
			{
				var nodes = new Node[A.Length];

				for (var i = 0; i < A.Length; i++)
				{
					nodes[i] = new Node(A[i]);
				}

				if (appendtype == AppendType.End)
				{
					foreach (var t in nodes)
					{
						l.AppendToTail(t);
					}
				}
				else
				{
					foreach (var t in nodes)
					{
						l.AppendToHead(t);
					}
				}
			}						

			return l;
		}

		public static void BuildLinkedListFromArrayOnTheFly(int[] A, Appender appendNodeToList)
		{			
			if (A.Length != 0)
			{
				var nodes = new Node[A.Length];

				for (var i = 0; i < A.Length; i++)
				{
					nodes[i] = new Node(A[i]);
				}
                
                foreach (var t in nodes)
                {
                    appendNodeToList(t);                
                }				
			}         			
		}


		// 2.1
		public bool RemoveDuplicates()
		{
			var dubs = new Hashtable();
			var tmp = Head;
			var result = false;

			while (tmp.Next != null)
			{
				if (dubs.ContainsKey(tmp.Next.Value))
				{
					tmp.Next = tmp.Next.Next;
					result = true;
				}
				else
				{
					dubs.Add(tmp.Next.Value, 1);
					tmp = tmp.Next;
				}

			}

			return result;
		}

		// 2.1 possible solution runner technique
		/*
				 * Remove Dups! Write code to remove duplicates from an unsorted linked list.
					FOLLOW UP
					How would you solve this problem if a temporary buffer is not allowed?
		 */

		public void RemoveDuplicatesWithoutTempBuffer()
		{

			Node slow = Head;
			Node fast = Head.Next;

			while (slow.Next != null)
			{
				while (fast.Next != null)
				{
					if (fast.Next.Value == slow.Next.Value)
					{
						fast.Next = fast.Next.Next;
					}
					else
					{
						fast = fast.Next;
					}
				}
				slow = slow.Next;
				fast = slow.Next;
			}

			// steps
			// use two pointers
			// keep one pointer in head 
			// sec p iterate through all elems and check for dup
			// move first pointer one pos next
		}

		// 2.2      Return Kth to Last: Implement an algorithm to find the kth to last element of a singly linked list.
		public Node KthToLast(int k)
		{
			var tmp = Head;
			var listLength = 0;

			while (tmp.Next != null)
			{
				listLength++;
				tmp = tmp.Next;
			}

			var count = listLength - k;
			tmp = Head;

			while (count > 0)
			{
				tmp = tmp.Next;
				count--;
			}

			return tmp.Next;
		}

		// 2.3
		/*
					 * Delete Middle Node: Implement an algorithm to delete a node in the middle (i.e., any node but
					 *   the first and last node, not necessarily the exact middle) of a singly linked list, given only access to
					 *  that node.
					 *  EXAMPLE
					 * lnput:the node c from the linked lista->b->c->d->e->f            
					 * Result: nothing is returned, but the new linked list looks like a->b->d->e- >f
		 * 
		 */

		public void DeleteMiddleNode(Node midNode)
		{
			midNode.Value = midNode.Next.Value;
			midNode.Next = midNode.Next.Next;
		}

		// 2.4
		/*Partition:    Write code to partition a linked list around a value x, such that all nodes less than x come
						before all nodes greater than or equal to x. If x is contained within the list, the values of x only need
						to be after the elements less than x (see below). The partition element x can appear anywhere in the
						"right partition"; it does not need to appear between the left and right partitions.
						EXAMPLE
						Input:
						Output:
						3 -> 5 -> 8 -> 5 -> 10 -> 2 -> 1 [partition= 5]
						3 -> 1 -> 2 -> 10 -> 5 -> 5 -> 8
		*/

		public void Partition(int x)
		{
			// iterate list
			// if node < x
			// extract node
			// attach to head

			var tmp = Head;
			while (tmp.Next != null)
			{
				if (tmp.Next.Value < x)
				{
					var extracted = tmp.Next;
					tmp.Next = tmp.Next.Next;
					AppendToHead(extracted);
				}
				else
				{
					tmp = tmp.Next;
				}
			}
		}

		// 2.5 
		//
		/*Sum Lists: You have two numbers represented by a linked list, where each node contains a single
		digit.The digits are stored in reverse order, such that the 1 's digit is at the head of the list. Write a
		function that adds the two numbers and returns the sum as a linked list.
		EXAMPLE
		Input: (7-> 1 -> 6) + (5 -> 9 -> 2).That is,617 + 295.
		Output: 2 -> 1 -> 9. That is, 912.
		FOLLOW UP
		Suppose the digits are stored in forward order. Repeat the above problem.
		EXAMPLE
		lnput:(6 -> 1 -> 7) + (2 -> 9 -> 5).That is,617 + 295.
		Output: 9 - > 1 -> 2. That is, 912.             
		*/
		
		public LinkedList SumLists(LinkedList l1, LinkedList l2)
		{
			var tmp1 = l1.Head;
			var tmp2 = l2.Head;
			var lresult = new LinkedList();
			var carrying = 0;

			while (tmp1.Next != null && tmp2.Next != null)
			{
				var d = tmp1.Next.Value + tmp2.Next.Value + carrying;
				var m = d / 10;

				if (m == 0)
				{
					var v = d;
					var vn = new Node(v);
					lresult.AppendToHead(vn);
				}
				else
				{
					var v = d % 10;
					carrying = 1;
					var vn = new Node(v);
					lresult.AppendToHead(vn);
				}
				tmp1 = tmp1.Next;
				tmp2 = tmp2.Next;
			}

			return lresult;

		}

		// 2.5 Final
		public LinkedList SumListsOfDifferentLength(LinkedList l1, LinkedList l2)
		{
			var len1 = l1.GetLength();
			var len2 = l2.GetLength();

			var diff = len1 - len2;


			if (diff > 0) // l2 smaller
			{
				for (var i = 0; i < diff; i++)
				{
					var n = new Node(0);
					l2.AppendToTail(n);
				}
			}
			else if (diff < 0)// l1 smaller
			{
				for (var i = 0; i < -diff; i++)
				{
					var n = new Node(0);
					l1.AppendToTail(n);
				}
			}

			return SumLists(l1, l2);
		}

		// Reverse a linkedlist
		public LinkedList ReverseLinkedList()
		{
			// 1->2->3->4            
			// 1<-2
			
			var reversed = new LinkedList();
			Node tmp = Head;
			while (tmp.Next != null)
			{
				reversed.AppendToHead(new Node(tmp.Value));    
				tmp = tmp.Next;
			}
			reversed.AppendToHead(new Node(tmp.Value));

			return reversed;
		}

	    public void ReverseListInPlace2()
	    {
	        Node p1 = null;
	        var p2 = Head;
	        var temp = new Node();
	        while (p2 != null)
	        {
	            temp = p2.Next;
	            p2.Next = p1;
	            p1 = p2;
	            p2 = temp;
	        }
	        Head = p1;
	    }


		// get Length of the list
		public int GetLength()
		{
			var tmp = Head;
			var length = 0;           

			while (tmp != null)
			{
				length++;
				tmp = tmp.Next;
			}
			return length;
		}

		// todo
		public bool Palindrome()
		{
            return false;
        }

		// todo
		public void Intersection()
		{
			throw new NotImplementedException();
		}

		// todo
		public void LoopDetection()
		{
			throw new NotImplementedException();
		}

		private static void Main(string[] args)
		{
		}
	}
}
