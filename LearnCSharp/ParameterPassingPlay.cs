﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CSharp
{
    public class Node {
        public int Val { get; set; }

        public Node()
        {
            Val = 0;
        }
    }
    public class ParameterPassingPlay
    {
        public static void ChangeArg(Node arg)
        {
            Node a2 = new Node();
            arg = a2;
            arg.Val = 3;
        }
        public static void ChangeArg2(Node arg)
        {
            Node a2 = new Node();
            arg = a2;
            a2.Val = 3;
        }
        public static void ChangeArg3(Node arg)
        {
            arg.Val = 3;
        }

       
    }

    [TestClass()]
    public class ParameterPassingPlayTests
    {
        [TestCategory("ParameterPassing")]
        [TestMethod()]
        public void ChangeArgTest()
        {
            var sut = new Node();

            ParameterPassingPlay.ChangeArg(sut);

            Assert.AreEqual(0, sut.Val);
        }

        [TestCategory("ParameterPassing")]
        [TestMethod()]
        public void ChangeArg2Test()
        {
            var sut = new Node();
            ParameterPassingPlay.ChangeArg2(sut);
            Assert.AreEqual(0, sut.Val);

        }

        [TestCategory("ParameterPassing")]
        [TestMethod()]
        public void ChangeArg3Test()
        {
            var sut = new Node();
            ParameterPassingPlay.ChangeArg2(sut);
            Assert.AreEqual(0, sut.Val);
        }
    }
}
