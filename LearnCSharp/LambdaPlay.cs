﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using static PIE_ARR_STR_Problems.Utilities;
using Newtonsoft.Json;


namespace PIE_ARR_STR_Problems
{
    public class LambdaPlay
    {
        delegate int Transformer(int i);

        static void Run()
        {
            Transformer sqr = x => x * x;
            Print(sqr(3));    // 9

            // Using a statement block instead:
            Transformer sqrBlock = x => { return x * x; };
            Print(sqr(3));

            // Using a generic System.Func delegate:
            Func<int, int> sqrFunc = x => x * x;
            Print(sqrFunc(3));

            // Using multiple arguments:
            //Func < 1, 2, outPut >

            Func <string, string, int> totalLength = (s1, s2) => s1.Length + s2.Length;
            int total = totalLength("hello", "worlds");
            total.Dump();
            //total.Dump("total");

            // Explicitly specifying parameter types:
            Func<int, int> sqrExplicit = (int x) => x * x;
            Print(sqrFunc(3));
            Pause();
        }
    }
}
