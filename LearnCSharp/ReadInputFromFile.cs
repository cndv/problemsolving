﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PIE_ARR_STR_Problems.Utilities;

namespace CSharp
{
    public class ReadInputFromFile
    {
        public void ParseFile()
        {
            // 1st way
            var path = System.Environment.CurrentDirectory;
            var filePath = $"{path}\\tinyGraph.txt";
            var text = System.IO.File.ReadAllText(filePath);

            System.Console.WriteLine("Contents of WriteText.txt : \n{0}", text);


            // 2nd Way
            var file =
                new System.IO.StreamReader(filePath);
            string line;
            while ((line = file.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }
            file.Close();

            // 3rd way
            using (var sr = new StreamReader(filePath))
            {
                string lineToRead;
                while ((lineToRead = sr.ReadLine()) != null)
                {
                    Println(lineToRead);
                }
            }
        }
    }
}
