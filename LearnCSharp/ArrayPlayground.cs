﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp
{
    public class ArrayPlayground
    {
        
        public int[] emptyArray()
        {
            var a = new int[10];
            for (int i = 0; i < 10; i++)
            {
                a[i] = i;
            }

            var index = 0;

            Console.WriteLine(index++);

            var aa = a.Take(3).Skip(1).Take(3);

            foreach (var i in aa)
            {
                Console.WriteLine(a[i]);
            }

            return a;
        }

    }
}
