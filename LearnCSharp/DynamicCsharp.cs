﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp
{
    class DynamicCsharp
    {
        private int _privateVar;

        public int MyVar
        {
            get { return _privateVar; }
            set { _privateVar = value; }
        }
        
    }

    class Test
    {
        static void Run(string[] args)
        {
            var x = "hello";
            Console.WriteLine(x.Length);


            var d = new DynamicCsharp();
            d.MyVar = 6;
            
            PIE_ARR_STR_Problems.Utilities.Print(d.MyVar);

            Console.ReadKey();


        }
    }

}
