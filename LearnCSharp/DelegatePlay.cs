﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using static PIE_ARR_STR_Problems.Utilities;
using Newtonsoft.Json;


namespace PIE_ARR_STR_Problems
{

    public class DelegatePlay
    {
        delegate int Transformer(int x);

        class Util
        {
            public static void Transform(int[] values, Transformer t)
            {
                for (int i = 0; i < values.Length; i++)
                    values[i] = t(values[i]);
            }
        }


    static void Run()
        {
            //----------------------------------------------------------------------------
            // example 1

            // To create a delegate instance, assign a method to a delegate variable:	
            Transformer t = Square;          // Create delegate instance
            var result = t(3);               // Invoke delegate caller
                                             //
            Print(result);      // 9

            //----------------------------------------------------------------------------
            // example 2

            int[] values = { 1, 2, 3 };
            Util.Transform(values, Square);      // Hook in the Square method
            values.Dump();

            values = new int[] { 1, 2, 3 };
            Util.Transform(values, Cube);        // Hook in the Cube method
            values.Dump();

            Pause();
        }

        static int Square(int x) => x^2;
        static int Cube(int x) => x^3;
    }
}
