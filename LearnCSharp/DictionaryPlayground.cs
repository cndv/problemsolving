﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CSharp
{
    class DictionaryPlayground
    {
        public void playWithHashMap()
        {
            var hs = new Dictionary<int, string>();
            hs.Add(1,"1");
            hs.Add(2,"2");
            hs.Add(4,"4");
            hs.Add(3,"3");

            
            foreach (KeyValuePair<int,string> pair in hs)
            {
                Console.WriteLine("{0}: {1}", pair.Key, pair.Value);
                Console.WriteLine(hs[pair.Key]);
            }
        }
    }

    [TestClass()]
    public class HashMapPlaygroundTests
    {
        [TestCategory("General")]
        [TestMethod()]
        public void playWithHashMapTest()
        {
            var sut = new DictionaryPlayground();
            sut.playWithHashMap();
        }
    }
}