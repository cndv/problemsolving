﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static PIE_ARR_STR_Problems.Utilities;


namespace CSharp
{
    public abstract class Foo
    {
        public bool DoSomething() { return false; }
        public abstract bool DoSthAbstractlly(); // mandatory to implement this method, cannot contain default implementation
        public virtual bool DoSthVirutally() { return false; } // optional to override this method, must have default implementation
    }

    public class Bar : Foo
    {
        public new bool DoSomething() { return true; } //'new' keyword is needed to allow you to 'override' non-virtual and static methods.
        public override bool DoSthAbstractlly() { return true; }
    }

    public class VirtualBar: Foo
    {
        public override bool DoSthAbstractlly() { return true; }

        public override bool DoSthVirutally() { return true; }
    }

    [TestClass()]
    public class FooTest
    {
        [TestMethod()]
        public void DoSthTest()
        {
            Bar sut = new Bar();
            Assert.IsTrue(sut.DoSomething());                          
            Assert.IsTrue(sut.DoSthAbstractlly());

            Foo sut2 = new VirtualBar();
            Assert.IsTrue(sut2.DoSthVirutally());
        }
    }
}
