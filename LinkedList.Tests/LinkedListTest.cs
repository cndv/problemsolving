// <copyright file="LinkedListTest.cs">Copyright ©  2016</copyright>
using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PIE_ARR_STR_Problems;

namespace PIE_ARR_STR_Problems.Tests
{
    /// <summary>This class contains parameterized unit tests for LinkedList</summary>
    [PexClass(typeof(LinkedList))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class LinkedListTest
    {
        /// <summary>Test stub for RemoveDuplicates()</summary>
        [PexMethod]
        public bool RemoveDuplicatesTest([PexAssumeUnderTest]LinkedList target)
        {
            bool result = target.RemoveDuplicates();
            return result;
            // TODO: add assertions to method LinkedListTest.RemoveDuplicatesTest(LinkedList)
        }
    }
}
