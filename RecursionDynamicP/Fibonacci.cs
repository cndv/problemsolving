﻿using System;

namespace RecursionDynamicP
{
    public class Fibonacci
    {
        /// <summary>
        ///     Calculate nth fib number
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public int FibonacciRecursive(int n)
        {
            if (n == 0 || n == 1)
            {
                return n;
            }
            return FibonacciRecursive(n-1) + FibonacciRecursive(n-2);
        }


        public int FibonacciMemoization(int n)
        {
            return FibonacciMemoization(n, new int[n+1]);
        }

        public int FibonacciMemoization(int i, int[] cache)
        {
            if (i == 0 || i == 1)
            {
                return i;
            }

            if (cache[i] == 0)
            {
                cache[i] = FibonacciMemoization(i - 1, cache) + FibonacciMemoization(i - 2, cache);
            }

            return cache[i];
        }

        public int FibonacciDynamicProgramming(int n)
        {
            if (n == 1 || n == 0)
            {
                return n;
            }


            int[] cache = new int[n];
            cache[0] = 0;
            cache[1] = 1;

            for (int i = 2; i < n; i++)
            {
                cache[i] = cache[i - 1] + cache[i - 2];
            }

            return cache[n-1] + cache[n-2];
        }


        public int FibonacciDynamicProgrammingNoArray(int n)
        {
            if (n == 1 || n == 0)
            {
                return n;
            }
            
            var a = 0;
            var b = 1;

            for (int i = 2; i < n; i++)
            {
                var c = a + b;
                a = b;
                b = c;
            }

            return a + b;
        }

    }
}
