﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PIE_ARR_STR_Problems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIE_ARR_STR_ProblemsTests
{
    [TestClass()]
    public class UtilitiesTests
    {
        [TestMethod()]
        public void CompareResultsFromFileTest()
        {
            string inputPath =
                "C:\\Users\\nskostas\\Documents\\_playground\\Workspace\\C# code\\Problem Solving\\CodePractice\\UtilitiesTests\\bin\\Debug\\input.txt";
            string outputPath = "C:\\Users\\nskostas\\Documents\\_playground\\Workspace\\C# code\\Problem Solving\\CodePractice\\UtilitiesTests\\bin\\Debug\\output.txt";
            var inData = Utilities.ReadFileFromPath(inputPath);
            var outData = Utilities.ReadFileFromPath(outputPath);

            CollectionAssert.AreEqual(inData, outData);
        }
    }
}