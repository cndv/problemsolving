﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Trees_Graphs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PIE_ARR_STR_Problems.Utilities;

namespace Trees_Graphs.Tests
{
    [TestClass()]
    public class BinarySearchTreeTests
    {
        [TestMethod()]
        public void getHeightOfTreeTest()
        {
            int[] a = { 10, 5, 11, 4, 6, 2 };

            var bst = new BinarySearchTree();
            bst.BuildBstFromArray(a);

            Assert.AreEqual(BinarySearchTree.GetHeightOfTree(bst.Root), 4);
        }

        [TestCategory("BST")]
        [TestMethod()]
        public void ValidateValidBstTest()
        {
            int[] A = { 10, 5, 11, 4, 6, 2 };
            var sut = new BinarySearchTree();

            sut.BuildBstFromArray(A);

            var result = sut.ValidateBst(sut.Root);
            Assert.IsTrue(result);
        }

        [TestCategory("BST")]
        [TestMethod()]
        public void ValidateInValidBstTest()
        {
            var twenty = new TreeNode(20);
            var ten = new TreeNode(10);
            var thirty = new TreeNode(30);
            var twentyFive = new TreeNode(25);

            var invalideBstTree = new BinarySearchTree()
            {
                Root = twenty
            };
            invalideBstTree.Root._left = ten;
            //invalideBstTree.Root._left._right = twentyFive;
            invalideBstTree.Root._right = thirty;
            invalideBstTree.Root._right._right = twentyFive;

            var result = invalideBstTree.ValidateBst(invalideBstTree.Root);
            Assert.IsFalse(result);
        }

        [TestCategory("BST")]
        [TestMethod()]
        public void FindNextInOrderSuccessorTest()
        {
            int[] A = { 10, 5, 11, 4, 6, 2 };
            var sut = new BinarySearchTree();

            sut.BuildBstFromArray(A);
            var n = sut.FindNode(6);
            var result = sut.FindNextInOrderSuccessor(n)._value == 10;

            Assert.IsTrue(result);
        }

        [TestCategory("BST")]
        [TestMethod()]
        public void FindNextInOrderSuccessorLastElementTest()
        {
            int[] A = { 10, 5, 11, 4, 6, 2 };
            var sut = new BinarySearchTree();

            sut.BuildBstFromArray(A);
            var n = sut.FindNode(11);
            var result = sut.FindNextInOrderSuccessor(n) == null;

            Assert.IsTrue(result);
        }


        [TestCategory("BST")]
        [TestMethod()]
        public void FindBSTSequencesTest()
        {
            int[] A = { 10, 5, 12, 11, 13, 6, 4 };
            var sut = new BinarySearchTree();
            sut.BuildBstFromArray(A);

            var result = sut.FindBSTSequences();

            Assert.IsFalse(true);

        }

        [TestCategory("General")]
        [TestMethod()]
        public void FindAllCombinationsTest()
        {
            int[] A = { 1, 2, 3 };
            var sut = new BinarySearchTree();

            var result = sut.FindAllCombinations(A);


        }
    }
}