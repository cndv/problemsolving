﻿using Trees_Graphs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Trees_Graphs.Tests
{
    [TestClass()]
    public class AVLTreeTests
    {
        [TestMethod()]
        [TestCategory("AVL")]

        public void BalanceFactorTest()
        {
            int[] a = { 10, 5, 11, 4, 6, 2 };

            var sut = new AVLTree();
            sut.BuildBstFromArray(a);

            Assert.AreEqual(AVLTree.BalanceFactor(sut.Root), -2);
        }

        [TestMethod()]
        [TestCategory("AVL")]

        public void CheckIfTreeIsBalancedTest()
        {
            int[] a = { 10, 5, 11, 4, 6, 2 };
            int[] b = {1, 2, 3, 4, 5, 6};

            var sutA = new AVLTree();
            sutA.BuildBstFromArray(a);
            Assert.IsTrue(AVLTree.CheckIfTreeIsBalanced(sutA.Root));

            var sutB = new AVLTree();
            sutB.BuildBstFromArray(b);
            Assert.IsTrue(AVLTree.CheckIfTreeIsBalanced(sutB.Root));
        }
    }
}