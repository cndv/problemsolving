﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PIE_ARR_STR_Problems;
using Trees_Graphs;
using static PIE_ARR_STR_Problems.Utilities;

namespace Trees_GraphsTests
{
    [TestClass()]
    public class GraphTests
    {
        [TestCategory("Graph")]
        [TestMethod()]
        public void BuildGraphFromArrayTest()
        {
            // Arrange
            var g = new Graph();
            var path = Environment.CurrentDirectory;
            var filePath = $"{path}\\tinyGraph.txt";

            // Act
            g.BuildGraphFromArray(filePath);
            var n = g.GetNode(0);
            var childExists = n.Children.FirstOrDefault(c => c.Value == 4);
            var result = childExists != null;

            // Assert
            g.PrintGraph();
            Assert.IsTrue(result);
        }

        [TestCategory("Graph")]
        [TestMethod()]
        public void DFSTest()
        {
            // Arrange
            var g = new Graph();
            var path = Environment.CurrentDirectory;
            var filePath = $"{path}\\tinyGraph.txt";
            g.BuildGraphFromArray(filePath);
            var startingNode = g.Nodes.Values.First();

            // Act
            g.DFS(startingNode);

            // Assert
            var expected = new int[] { 0, 1, 3, 2, 4, 5 };
            CollectionAssert.AreEqual(g.VisitedNodes, expected);
        }

        [TestCategory("Graph")]
        [TestMethod()]
        public void BFSTest()
        {
            // Arrange
            var g = new Graph();
            var path = Environment.CurrentDirectory;
            var filePath = $"{path}\\tinyGraph.txt";
            g.BuildGraphFromArray(filePath);
            var startingNode = g.Nodes.Values.First();

            // Act
            g.BFS(startingNode);

            // Assert
            var expected = new int[] { 0, 1, 4, 5, 3, 2 };
            CollectionAssert.AreEqual(g.VisitedNodes, expected);
        }

        [TestCategory("Graph")]
        [TestMethod()]
        public void CheckIfRouteExistsBetweenNodesTest()
        {
            // Arrange
            var g = new Graph();
            var path = Environment.CurrentDirectory;
            var filePath = $"{path}\\tinyGraph.txt";
            g.BuildGraphFromArray(filePath);
            var a = g.GetNode(0);
            var b = g.GetNode(3);

            // Act
            var result = g.CheckIfRouteExistsBetweenNodes(a, b);

            Assert.IsTrue(result);
        }

        [TestMethod()]
        [TestCategory("Graph")]
        public void BFSShortestReachTest()
        {
            var g = new Graph();
            string result = g.BFSShortestReach();
            Console.WriteLine(result);
            Assert.AreEqual("66-1\n-16\n", result);
        }

        [TestMethod()]
        [TestCategory("Graph ShortestPath")]
        public void ShortestReachDijkstraTest()
        {
            var g = new Graph();
            string result = g.DijkstraShortestReach();
            Assert.Fail();
        }
    }
}